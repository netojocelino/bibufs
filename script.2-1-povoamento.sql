﻿SET search_path to bibufs;




--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 
--------------------- ---------------------    DADOS   USUÁRIO    --------------------- --------------------- --------------------- 
--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 

--------------------- ENDEREÇOS
INSERT INTO Endereco (pais, estado, cidade, bairro, rua, numero, complemento, cep)
    VALUES
    ('Brasil', 'Sergipe', 'São Cristóvão', 'Rosa Elze', 'Rod João Bebe Água', 7561, 'Sergipe Tec', '49100000');
    
INSERT INTO Endereco (pais, estado, cidade, bairro, rua, numero, complemento, cep)
    VALUES
    ('Brasil', 'Sergipe', 'São Cristóvão', 'Eduardo Gomes', 'Av Ayrton Senna', 0, 'GBarbosa', '49100000');
    
INSERT INTO Endereco (pais, estado, cidade, bairro, rua, numero, complemento, cep)
    VALUES
    ('Brasil', 'Sergipe', 'Nossa Senhora da Glória', 'Centro', 'Dom Pedro II', 742, 'Secretária de Obras', '49680000');
    
INSERT INTO Endereco (pais, estado, cidade, bairro, rua, numero, complemento, cep)
    VALUES
    ('Brasil', 'Sergipe', 'Aracaju', 'Ponto Novo', 'Av Augusto Franco', 364, 'GBarbosa', '49047040');





--------------------- PESSOAS
INSERT INTO Pessoa (cpf, nome, nascimento, telefone, email, sexo, endereco)
  VALUES
('06874724450', 'Neto Alves', '1996-09-16', '79998975861', 'netoalves@email.com', 'm', 1);

INSERT INTO Pessoa (cpf, nome, nascimento, telefone, email, sexo, endereco)
  VALUES
('06879514000', 'Netinho Alves', '1996-09-14', '79998972861', 'netinhoalves@email.com', 'm', 2);

INSERT INTO Pessoa (cpf, nome, nascimento, telefone, email, sexo, endereco)
  VALUES
('06879514169', 'Jocelino Alves', '1996-09-06', '79995675861', 'jocelinoalves@email.com', 'm', 3);

INSERT INTO Pessoa (cpf, nome, nascimento, telefone, email, sexo, endereco)
  VALUES
('06587514450', 'J Alves', '1996-09-26', '79998972361', 'jalves@email.com', 'm', 4);

INSERT INTO Pessoa (cpf, nome, nascimento, telefone, email, sexo, endereco)
  VALUES
('06058914450', 'N Alves', '1996-09-01', '79998975151', 'nalves@email.com', 'm', 2);





--------------------- COLABORADOR
INSERT INTO Colaborador (cpf) VALUES ('06874724450');

INSERT INTO Colaborador (cpf) VALUES ('06879514000');

INSERT INTO Colaborador (cpf) VALUES ('06879514169');






--------------------- FUNCIONARIO

INSERT INTO Funcionario (salario, cargahoraria, cargo, cpf)
    VALUES
(1000, 40, 'Repositor', '06874724450');


INSERT INTO Funcionario (salario, cargahoraria, cargo, cpf)
    VALUES
(1000, 40, 'Repositor', '06879514000');









--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 
--------------------- ---------------------     DADOS   LIVROS    --------------------- --------------------- --------------------- 
--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 

--------------------- EDITORA

INSERT INTO editora
  (nome, telefone, email)
    VALUES
  ('Buguinhos', '9998477857', 'buguinhos@editora.com');


INSERT INTO editora
  (nome, telefone, email)
    VALUES
  ('PostBooks', '793411515479', 'postbooks@editora.com');


INSERT INTO editora
  (nome, telefone, email)
    VALUES
  ('Atominho', '793641249752', 'atominho@editora.com');


INSERT INTO editora
  (nome, telefone, email)
    VALUES
  ('firebook', '798564285730', 'firebook@editora.com');

  
INSERT INTO editora
  (nome, telefone, email)
    VALUES
  ('javarticle', '793475668899', 'javarticle@editora.com');


  





--------------------- AUTOR

INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Brendam Eich', 'Finlândia', '1969-12-28', 'm');

  
INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Linus Torvalds', 'Estados Unidos', '1961-07-04', 'm');
-- https://www.google.com.br/search?q=linus+torvalds+livros&stick=H4sIAAAAAAAAAONgFuLQz9U3MEnPS1OCs7SkspOt9JPy87P1E0tLMvKLrEDsYoX8vJxKAOAJcIMyAAAA&sa=X&ved=2ahUKEwiftuWbiZbdAhWBDJAKHdV9BTwQMSgAMBt6BAgGED4&biw=1280&bih=931


  
INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Zélia Gattai', 'Brasil', '1916-07-02', '2');

  
INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Grace Hopper', 'Estados Unidos', '1906-12-09', 'f');


INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Gillian Flynn', 'Estados Unidos', '1971-02-24', 'f');

  
INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Neil Gaiman', 'Reino Unido', '1960-11-10', 'm');


INSERT INTO autor
  (nome, pais, nascimento, sexo)
    VALUES
  ('Eduardo Spohr', 'Brasil', '1976-06-05', 'm');

















---------------------  PUBLICACAO



INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
  ('9781565925823','Open Sources',': Voices from the Open Source Revolution',1999,1,2,2);


INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ('9788535913910', 'ANARQUISTAS', ', GRAÇAS A DEUS', 2009, 1, 3, 3);

INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ( '9780314665904', 'Understanding Computers', '', 1990, 3, 2, 4);

INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ( '9788580572902', 'Garota Exemplar', '', 2013, 1, 4, 5);

INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ( '9788551000724', 'Deuses Americanos', '', 2016, 1, 1, 6);

INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ( '9788576861416', 'Filhos Do Eden', 'Herdeiros De Atlântida - Vol 1', 2011, 5, 4, 7);






/*

INSERT INTO publicacao
  (isbn, titulo, subtitulo, ano, edicao, ideditora, idautor)
    VALUES
    ( '9788576861412', 'Filhos De Satan', 'Herdeiros De Lucifer ', 1, 2018, NULL, NULL);

    */

/*
UPDATE publicacao
  SET ano = 2011, edicao = 5
    WHERE isbn = '9788576861416';
*/

    
--------------------- ASSUNTO
INSERT INTO  assunto (nome) VALUES ('Ficção Científica');
INSERT INTO  assunto (nome) VALUES ('Romance');
INSERT INTO  assunto (nome) VALUES ('Tecnologia');
INSERT INTO  assunto (nome) VALUES ('Acadêmico');
INSERT INTO  assunto (nome) VALUES ('Memórias');
INSERT INTO  assunto (nome) VALUES ('Autobiografia');
INSERT INTO  assunto (nome) VALUES ('Biografia');
INSERT INTO  assunto (nome) VALUES ('Aventura');
INSERT INTO  assunto (nome) VALUES ('Mitologia');
INSERT INTO  assunto (nome) VALUES ('Suspense');
INSERT INTO  assunto (nome) VALUES ('Antologia');
INSERT INTO  assunto (nome) VALUES ('Computação');






--------------------- PRATELEIRA




INSERT INTO  prateleira (tags) VALUES ('Ficção Científica');
INSERT INTO  prateleira (tags) VALUES ('Romance');
INSERT INTO  prateleira (tags) VALUES ('Tecnologia');
INSERT INTO  prateleira (tags) VALUES ('Acadêmico');
INSERT INTO  prateleira (tags) VALUES ('Memórias');
INSERT INTO  prateleira (tags) VALUES ('Autobiografia');
INSERT INTO  prateleira (tags) VALUES ('Biografia');
INSERT INTO  prateleira (tags) VALUES ('Aventura');
INSERT INTO  prateleira (tags) VALUES ('Mitologia');
INSERT INTO  prateleira (tags) VALUES ('Suspense');
INSERT INTO  prateleira (tags) VALUES ('Antologia');
INSERT INTO  prateleira (tags) VALUES ('Computação');






--------------------- EXEMPLAR
INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9781565925823, 11);
 
INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788535913910, 5);

INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9780314665904, 12);

INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788580572902, 10);
 
INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788580572902, 10);
 
INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788580572902, 10);

INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788551000724 ,2);

INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788551000724 ,2);

INSERT INTO exemplar (idpublicacao, idprateleira)
 VALUES (9788576861416, 9);





--------------------- Assunto has Publicacao

INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9781565925823', 3);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9781565925823', 11);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9781565925823', 12);



INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788535913910', 5);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788535913910', 7);



INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9780314665904', 3);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9780314665904', 4);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9780314665904', 12);



INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788580572902', 2);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788580572902', 10);



INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788551000724', 8);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788551000724', 9);


INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788576861416', 8);
INSERT INTO AssuntohasPublicacao
 (isbn, idassunto)
VALUES ('9788576861416', 9);








--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 
------------------- ---------------------     DADOS   GERENCIA    --------------------- --------------------- ---------------------
--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 

--------------------- Emprestimo
INSERT INTO emprestimo
 (dataemprestimo, datalimite, datadevolucao, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2018-06-11', '2018-07-01',3, 1, 1);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite, datadevolucao, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2018-01-11', '2018-01-20',3, 1, 2);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite, datadevolucao, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2015-06-11', '2015-06-16',3, 1, 3);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite,  idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2017-06-01','2017-06-11',1, 2, 4);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2018-04-11',1, 1, 5);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2018-06-11',2, 2, 6);
 
INSERT INTO emprestimo
 (dataemprestimo, datalimite, datadevolucao, idcolaborador, idfuncionario, idexemplar)
   VALUES
 ('2018-06-01','2018-06-11', '2019-12-11',2, 2, 7);

SELECT idexemplar
FROM exemplar
WHERE
  idpublicacao = '9788551000724'
	AND
  disponivel = true
LIMIT 1




--------- MOLDE PARA EMPRESTIMO
INSERT INTO emprestimo
  (dataemprestimo, datalimite, idcolaborador, idfuncionario, idexemplar)
    VALUES
  (now(), (NOW() + INTERVAL '10 days'), 3, 2, (SELECT idexemplar
		FROM exemplar
		WHERE
		  idpublicacao = '9788551000724'
			AND
		  disponivel = true
		LIMIT 1)
);




 SELECT * from emprestimo ORDER BY dataemprestimo DESC
 SELECT * from exemplar where idexemplar = 8
-- SELECT * FROM emprestimo;

--------------------- Multas

WITH with_datas AS (SELECT idemprestimo, dataemprestimo AS a,datadevolucao AS b
                    FROM emprestimo WHERE (datadevolucao IS NOT NULL) AND (idemprestimo = 2) )
,    diferecas AS (SELECT idemprestimo, abs(b::date - a::date) AS diff FROM  with_datas)


INSERT INTO multa
 (valor, idemprestimo)
   VALUES
 (    ((SELECT diff FROM diferecas LIMIT 1)* 2), (SELECT idemprestimo FROM diferecas LIMIT 1 ));

UPDATE emprestimo
  SET idmulta = (SELECT idmulta FROM multa ORDER BY idmulta DESC LIMIT 1)
  WHERE idemprestimo = 2;







--------------------- Reserva
INSERT INTO reserva
  (hora, idpublicacao, idcolaborador)
    VALUES
  (now(), '9781565925823', 2);



INSERT INTO reserva
  (hora, idpublicacao, idcolaborador)
    VALUES
  (now(), '9788580572902', 1);


  
--SELECT * FROM reserva;