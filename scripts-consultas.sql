﻿SET search_path TO bibufs;

--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 
--------------------- ---------------------         ACERVO       --------------------- --------------------- --------------------- 
--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 



--------------------- CONSULTAR LIVRO POR TÍTULO
SELECT (p.isbn || '    ' || p.titulo ||' '|| p.subtitulo || '  ' || '(' || p.ano || ')    ' || p.edicao || 'ª Edição' || '  ' || a.nome) AS livro
  FROM publicacao AS p
  JOIN autor      AS a
    ON (p.idautor = a.idautor)
  WHERE p.titulo iLIKE '%americanos%';





--------------------- CONSULTAR LIVRO POR AUTOR
SELECT (p.isbn || '    ' || p.titulo ||' '|| p.subtitulo || '  ' || '(' || p.ano || ')    ' || p.edicao || 'ª Edição' || '  ' || a.nome) AS livro
  FROM publicacao AS p
  JOIN autor      AS a
    ON (p.idautor = a.idautor)
  WHERE a.nome iLIKE '%gai%';






--------------------- CONSULTAR LIVRO E MOSTRAR QUTD DISPONIVEL
SELECT (p.isbn || '    ' || p.titulo ||' '|| p.subtitulo || '  ' || '(' || p.ano || ')    ' ||
    p.edicao || 'ª Edição' || '  ' || a.nome || ' ' || ' exemplar cod ' || e.idexemplar ||
    '  prateleira ' || pr.tags || ' disponivel ' || e.disponivel
    ) AS livro
  FROM publicacao AS p
  JOIN autor      AS a
    ON (p.idautor = a.idautor)
  JOIN exemplar e
    ON (p.isbn = e.idpublicacao)
  JOIN prateleira pr
    ON (e.idprateleira = pr.idprateleira)
  WHERE p.titulo iLIKE '%americanos%';




 --------------------- CONSULTAR LIVROS COM MULTAS
SELECT 
    pes.nome, emp.idemprestimo, pub.titulo || ' ' || pub.subtitulo AS titulo, mul.valor::float8::numeric::money AS multa
FROM colaborador AS col
  JOIN pessoa AS pes
    USING (cpf)
  JOIN emprestimo  AS emp
    USING (idcolaborador)
  JOIN multa AS mul
    USING (idemprestimo)
  JOIN exemplar AS exp
    USING (idexemplar)
   JOIN publicacao AS pub
     ON(exp.idpublicacao = pub.isbn)
ORDER BY col.idcolaborador ASC;






-- LISTAR LOCALIZAÇÃO DO EXEMPLAR
SELECT
  prat.idprateleira, pub.titulo, pub.subtitulo, pub.ano
FROM
  publicacao AS pub
  JOIN
    exemplar AS exp
  ON
    (pub.isbn = exp.idpublicacao)
  JOIN
    prateleira AS prat
  USING
    (idprateleira)
  JOIN
    assuntohaspublicacao AS ahp
  USING
   (isbn)
  JOIN
    assunto AS ass
  USING (idassunto);





--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 
--------------------- ---------------------         ITERAÇÕES       --------------------- --------------------- --------------------- 
--------------------- --------------------- --------------------- --------------------- --------------------- --------------------- 


  


--- Alterar disponivel nos exemplares
UPDATE exemplar
  SET disponivel = false
WHERE idexemplar IN 
(
	SELECT idexemplar
	FROM exemplar AS ex
	  WHERE ex.idexemplar IN (
	  SELECT idemprestimo
	FROM colaborador col
	  JOIN emprestimo emp
	    ON (emp.idcolaborador = col.idcolaborador)
	WHERE emp.datadevolucao IS NULL
	)
);




--
--  LER EXEMPLARES DISPONIVEIS PARA  EMPRESTAR
WITH disponiveis AS (
	SELECT pub.isbn, exp.idexemplar
	FROM publicacao       AS pub
	  FULL JOIN exemplar  AS exp
	    ON (pub.isbn = exp.idpublicacao)
	  WHERE
	    (exp.disponivel = true)
		AND
	    (pub.titulo iLIKE '%deuses%')
	 LIMIT 1
)

INSERT INTO emprestimo
 (dataemprestimo, datalimite, idcolaborador, idfuncionario, idexemplar)
   VALUES
 (now(),(NOW() + INTERVAL '10 days'), 2, 2, (SELECT idexemplar FROM disponiveis));



SELECT * FROM disponiveis;
SELECT * FROM emprestimo;
SELECT * FROM colaborador;
SELECT * FROM funcionario;


  


-- Realizar emprestimo
SELECT * FROM publicacao AS pub, exemplar AS exp
  WHERE pub.isbn = '';
  




-- listar reservados
SELECT *
FROM emprestimo         AS emp
  NATURAL JOIN exemplar AS exp
  RIGHT JOIN reserva  AS res
    USING (idpublicacao)
ORDER BY idexemplar;




-- listar reservados disponiveis
SELECT ps.nome, plc.isbn, plc.titulo, plc.subtitulo
FROM emprestimo         AS emp
  NATURAL JOIN exemplar AS exp
  RIGHT JOIN reserva    AS res
    USING (idpublicacao)
  JOIN colaborador      AS col
    ON (col.idcolaborador = res.idcolaborador)
  JOIN pessoa           AS ps
    ON (ps.cpf = col.cpf)
  JOIN publicacao       AS plc
    ON (plc.isbn = exp.idpublicacao)
WHERE disponivel = true
ORDER BY idexemplar;




/*

SELECT * FROM pessoa
SELECT * FROM colaborador
SELECT * FROM reserva
SELECT * FROM publicacao
SELECT * FROM emprestimo  
SELECT * FROM exemplar where idpublicacao = '9788580572902'
SELECT * FROM prateleira
SELECT * FROM assunto
SELECT * FROM assuntohaspublicacao*/