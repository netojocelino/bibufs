﻿----------------------------------------------------------------------------------
-- consulta que pega a última informação de um emprestimo de um dado livro
WITH dados_todos_emprestimo_id AS (SELECT idlivro, emprestimos
                         FROM "public"."livro_registrado" le
                         WHERE (le.idlivro=1) AND (le.emprestimos IS NOT NULL)
                         ORDER BY le.momento DESC LIMIT 1)

WITH ultimo_emprestimo AS (SELECT e.idlivro, due.emprestimos
FROM emprestimo AS e , (SELECT * FROM dados_ultimo_emprestimo) AS due
  WHERE e.idlivro = due.idlivro
  ORDER BY e.momento DESC
  LIMIT 1)
----------------------------------------------------------------------------------





















































---- SABER SE UM EMPRESTIMO É VAZIO
WITH pegar_ultimo_emprestimo AS (
    SELECT idlivro, COALESCE(NULLIF(emprestimos, NULL), 0) AS emprestimos, momento
	FROM livro_registrado    WHERE idlivro=1    ORDER BY momento DESC    LIMIT 1
)
---- PEGAR O ÚLTIMO NUMERO DE EMPRESTIMOS E INCREMENTAR UM NA VIEW
,emprestimo_incrementado AS (SELECT (1+emprestimos) as emp from pegar_ultimo_emprestimo)
,emprestimo_nao_incrementado AS (SELECT emprestimos as emp from pegar_ultimo_emprestimo)
SELECT emp FROM emprestimo_incrementado;



INSERT INTO livro_registrado
 (idlivro, emprestimos)
   VALUES 
  (11, (SELECT emp FROM emprestimo_incrementado));
  SELECT * FROM livro_registrado ORDER BY momento DESC;
   


----------------------------------------------------------------------------------




DROP TRIGGER update_emprestado ON emprestimo;

CREATE OR REPLACE FUNCTION livro_emprestado_func()
 RETURNS trigger AS $trigger_emprestado$
 BEGIN
   ---- SABER SE UM EMPRESTIMO É VAZIO
	WITH pegar_ultimo_emprestimo AS (
	    SELECT idlivro, COALESCE(NULLIF(emprestimos, NULL), 0) AS emprestimos, momento
		FROM livro_registrado    WHERE idlivro=NEW.idlivro    ORDER BY momento DESC    LIMIT 1
	)
	---- PEGAR O ÚLTIMO NUMERO DE EMPRESTIMOS E INCREMENTAR UM NA VIEW
	,emprestimo_incrementado AS (SELECT (1+emprestimos) as emp from pegar_ultimo_emprestimo)
	,emprestimo_nao_incrementado AS (SELECT emprestimos as emp from pegar_ultimo_emprestimo)
    ------ defaut
  INSERT INTO livro_registrado
   (momento, idlivro, emprestimos)
      VALUES
   (NEW.momento, NEW.idlivro, (SELECT emp FROM emprestimo_incrementado));
  RETURN NEW;
 END;
 $trigger_emprestado$ LANGUAGE plpgsql;

CREATE TRIGGER update_emprestado
  AFTER INSERT ON emprestimo
  FOR EACH ROW
 EXECUTE PROCEDURE livro_emprestado_func();
 
 
 
 
 
 
 
 
 
 
 -----------------*------------------------
 SELECT * FROM emprestimo;
 SELECT * FROM livro;
 
 INSERT INTO livro
  (nome)
    VALUES
  ('gone girl');
 
 
 SELECT * FROM livro_registrado WHERE idlivro = 4
 
 INSERT INTO emprestimo
 (idlivro)
   VALUES
 (4);
 SELECT * FROM livro_registrado WHERE idlivro = 4 ORDER BY momento DESC;