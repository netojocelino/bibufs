﻿CREATE SCHEMA IF NOT EXISTS bibufs;

SET search_path to bibufs;


CREATE DOMAIN tipoCPF AS CHAR(11);
CREATE DOMAIN tipoSEXO as CHAR(1) DEFAULT 'm';

--
CREATE TABLE Endereco(
    idEndereco SERIAL NOT NULL,
    pais VARCHAR(45) NOT NULL,
    estado VARCHAR(45) NOT NULL,
    cidade VARCHAR(45) NOT NULL,
    bairro VARCHAR(45) NOT NULL,
    rua VARCHAR(45) NOT NULL,
    numero SMALLINT NOT NULL,
    complemento VARCHAR(45) NOT NULL,
    cep CHAR(8) NOT NULL,

    CONSTRAINT pk_idEndereco PRIMARY KEY (idEndereco),
    CONSTRAINT uq_endereco UNIQUE (cep, rua, bairro, cidade, estado, numero)
);

--
CREATE TABLE Pessoa(
    cpf tipoCPF,
    nome VARCHAR(45) NOT NULL,
    nascimento DATE NOT NULL,
    telefone CHAR(12) NOT NULL,
    email VARCHAR(45) NOT NULL,
    sexo tipoSEXO,
    endereco INT,
    
    CONSTRAINT pk_cpf PRIMARY KEY (cpf),
    CONSTRAINT fk_endereco FOREIGN KEY (endereco)
      REFERENCES endereco(idEndereco)
      ON DELETE SET NULL
      ON UPDATE CASCADE,
    CONSTRAINT uq_pessoa UNIQUE (cpf,nome,nascimento,telefone,email)
);

--
CREATE TABLE Colaborador(
    idColaborador SERIAL,
    cpf tipoCPF UNIQUE, -- IS NOT NULL, -- Passivel de deivar ~null, pra garantir de ligar com pessoa
    --CONSTRAINT uq_colaborador UNIQUE cpf,
    CONSTRAINT pk_idColaborador PRIMARY KEY (idColaborador),
    CONSTRAINT fk_cpf FOREIGN KEY (cpf)
      REFERENCES Pessoa(cpf)
      ON DELETE SET NULL
);

--
CREATE TABLE Funcionario(
    idFuncionario SERIAL NOT NULL, -- Passivel deivar null, por ser serial
    salario INT CHECK (salario >= 954 AND salario <= 5000),
    cargaHoraria SMALLINT CHECK(cargaHoraria >= 30 AND cargaHoraria <= 60),
    cargo VARCHAR(45),
    cpf tipoCPF UNIQUE,
    CONSTRAINT pk_idFuncionario PRIMARY KEY (idFuncionario),
    CONSTRAINT fk_pessoa FOREIGN KEY (cpf)
      REFERENCES Pessoa(cpf)
      ON DELETE SET NULL

);

--
CREATE TABLE Assunto(
  idAssunto SERIAL NOT NULL, -- passivel deixar null, por ser serial
  nome VARCHAR(45),

  CONSTRAINT uq_assunto UNIQUE (nome),
  CONSTRAINT pk_assunto PRIMARY KEY (idAssunto)
);

--
CREATE TABLE Editora(
    idEditora SERIAL NOT NULL, -- passivel deixar null por ser serial
    nome VARCHAR(45) NOT NULL,
    telefone CHAR(12) NOT NULL,
    email VARCHAR(45) NOT NULL,
    CONSTRAINT pk_idEditora PRIMARY KEY (idEditora)
    -- , CONSTRAINT uq_editora UNIQUE (nome, telefone)  ---- INFORMAÇÃO: UMA EDITORA PODE TER UMA SUBSIDIARIA MAS COM OUTRO TELEFONE
);

-- 
CREATE TABLE Autor(
    idAutor SERIAL NOT NULL,-- passivel deixar null por ser serial
    nome VARCHAR(45) NOT NULL,
    pais VARCHAR(45) NOT NULL,
    nascimento DATE NOT NULL,
    sexo tipoSEXO,
    CONSTRAINT pk_idAutor PRIMARY KEY (idAutor),
    CONSTRAINT uq_autor UNIQUE (nome, nascimento)
);

--
CREATE TABLE Publicacao(
    isbn CHAR(13) NOT NULL, -- passivel mudar pra char(13)
    titulo VARCHAR(45) NOT NULL,
    subTitulo VARCHAR(45) NOT NULL,
    ano SMALLINT NOT NULL CHECK(ano <= DATE_PART('year',now())), -- passivel deixar <=
    edicao INT DEFAULT 1,
    idEditora INT NOT NULL,
    idAutor INT NOT NULL,
    /*--KEYS--*/
    CONSTRAINT pk_isbn PRIMARY KEY (isbn),
    CONSTRAINT fk_editora FOREIGN KEY (idEditora)
      REFERENCES editora(idEditora)
      ON UPDATE CASCADE,
    CONSTRAINT fk_idAutor FOREIGN KEY (idAutor)
      REFERENCES autor(idAutor)
      ON UPDATE CASCADE
	-- , CONSTANT uq_pub UNIQUE (isbn, edicao) -- garatir que se aedição for diferente o isbn também
);

--
CREATE TABLE AssuntoHasPublicacao(
  idAssunto INT NOT NULL,
  isbn CHAR(13) NOT NULL,

  CONSTRAINT pk_idAssuntoIsbn PRIMARY KEY(idAssunto,isbn),
  CONSTRAINT fk_idAssunto FOREIGN KEY (idAssunto) REFERENCES Assunto(idAssunto),
  CONSTRAINT fk_isbn FOREIGN KEY (isbn) REFERENCES Publicacao(isbn)
);

--
CREATE TABLE Prateleira(      --INFELIZMENTE NAO TENHO UMA UTILIDADE PRA ESSA TABELA AINDA
    idPrateleira SERIAL NOT NULL,
    tags VARCHAR(45) NOT NULL,
    CONSTRAINT pk_idPrateleira PRIMARY KEY (idPrateleira),
	CONSTRAINT fk_tagPrateleira   FOREIGN KEY (tags) REFERENCES Assunto(nome)
    
);


--
CREATE TABLE Exemplar(
    idExemplar SERIAL NOT NULL,
    idPublicacao CHAR(13) NOT NULL,
    idPrateleira INT NOT NULL,
    disponivel BOOLEAN DEFAULT TRUE,

    CONSTRAINT pk_idExemplar PRIMARY KEY(idExemplar),
    CONSTRAINT fk_idPublicacao FOREIGN KEY (idPublicacao)
      REFERENCES publicacao(isbn)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    CONSTRAINT fk_idPrateleira FOREIGN KEY (idPrateleira)
      REFERENCES prateleira(idPrateleira)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);


--
/*
	CREATE TABLE emprestimo(
	    idEmprestimo SERIAL,
	    dataEmprestimo DATE NOT NULL,
	    dataLimite DATE CHECK (dataLimite <= (dataEmprestimo+10)),  -- DATA LIMITE SER A DATA DE EMPRESTIMO MAIS
	    dataDevolucao DATE,
	    idMulta INT,
	    idColaborador INT NOT NULL,
	    idExemplar INT NOT NULL,
	    -- idISBN INT,
	    CONSTRAINT pk_idEmprestimo PRIMARY KEY(idEmprestimo),
	    CONSTRAINT fk_idColaborador FOREIGN KEY (idColaborador) REFERENCES colaborador(idColaborador) ON DELETE CASCADE ON UPDATE CASCADE
	);
*/
CREATE TABLE Emprestimo(
    idEmprestimo SERIAL,
    dataEmprestimo DATE NOT NULL DEFAULT now(),
    dataLimite DATE
          CHECK   (dataEmprestimo <= ( now() + INTERVAL '10 days')),
         -- DEFAULT (now() + INTERVAL '10 days')  /*DATA LIMITE SER A DATA DE EMPRESTIMO MAIS &*/
    dataDevolucao DATE,
    idMulta INT,
    idColaborador INT NOT NULL,
    idFuncionario INT NOT NULL,
    idExemplar INT NOT NULL,
    -- idISBN INT,
    CONSTRAINT pk_idEmprestimo  PRIMARY KEY (idEmprestimo),
    CONSTRAINT fk_idColaborador FOREIGN KEY (idColaborador)
               REFERENCES colaborador(idColaborador)
               ON DELETE CASCADE
               ON UPDATE CASCADE,
    CONSTRAINT fk_idFuncionario FOREIGN KEY (idFuncionario)
               REFERENCES funcionario(idFuncionario)
               ON DELETE CASCADE
               ON UPDATE CASCADE,
    CONSTRAINT ck_emprestimo    CHECK       (datalimite <= (dataemprestimo + (INTERVAL '10 days')))
);






CREATE TABLE Multa(
    idMulta SERIAL,
    valor INT NOT NULL,    /*PROVAVELMENTE SERIA INTERESSANTE UTILIZAR O CHECK PARA CALCULAR O VALOR DA MULTA COM BASE NAS DATAS DE DEVOLUCAO E DATA LIMITE, POREM NAO SEI SE EH VALIDO*/
    dataPagamento DATE,
    idEmprestimo INT,
    CONSTRAINT pk_idMulta PRIMARY KEY(idMulta),
    CONSTRAINT fk_idEmprestimo FOREIGN KEY (idEmprestimo) REFERENCES emprestimo(idEmprestimo) ON DELETE CASCADE ON UPDATE CASCADE

);
CREATE TABLE Reserva(
    idReserva SERIAL,
    hora TIMESTAMP NOT NULL,
    ativa BOOLEAN NOT NULL DEFAULT(TRUE),  
    idPublicacao CHAR(13),
    idColaborador INT,
    CONSTRAINT pk_idReserva PRIMARY KEY(idReserva),
    CONSTRAINT fk_idPublicacao FOREIGN KEY (idPublicacao) REFERENCES publicacao(isbn) ON DELETE CASCADE ON UPDATE CASCADE, /***NO CASCADE PROBABLY***/
    CONSTRAINT fk_idColaborador FOREIGN KEY (idColaborador) REFERENCES colaborador(idColaborador) ON DELETE CASCADE ON UPDATE CASCADE /***NO CASCADE PROBABLY***/
);


-------------
-------------------------- TRIGGER PARA QUANDO FOR PEGAR EXEMPLAR
-------------
CREATE OR REPLACE FUNCTION alterar_disponivel_exemplar()
  RETURNS trigger AS $exemplar_disponivel_trigger$
  BEGIN
    UPDATE exemplar
      SET disponivel = false
    WHERE idexemplar = NEW.idexemplar;
    RETURN NEW;
  END;
$exemplar_disponivel_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER exemplar_disponivel_trigger
  AFTER INSERT ON emprestimo
  FOR EACH ROW
  EXECUTE PROCEDURE alterar_disponivel_exemplar();
  ---------------------